package cl.ubb.Pila;

public class Pila {
	
    public Nodo inicio;
    private int tamanio;
    
    public void Pila(){
        inicio = null;
        tamanio = 0;
    }
    
    public boolean esVacia(){
        return inicio == null;
    }
    
    public int getTamanio(){
        return tamanio;
    }
    
    public void push(int valor){
        Nodo nuevo = new Nodo();
        nuevo.setValor(valor);
        if (esVacia()) {
            inicio = nuevo;
        }
        else{
            nuevo.setSiguiente(inicio);
            inicio = nuevo;
        }
        tamanio++;
    } 
    
    public int pop(){
    	int valor = inicio.getValor();
        if (!esVacia()) {
            inicio = inicio.getSiguiente();
            tamanio--;
            return valor;
        }
        return valor;
    }
    
    public int top(){
        if(!esVacia()){
            return inicio.getValor();
        }
		return inicio.getValor(); 
    }
    
    public void listar(){
        Nodo aux = inicio;
        while(aux != null){
            aux.getValor();
            aux = aux.getSiguiente();
        }
    }
}