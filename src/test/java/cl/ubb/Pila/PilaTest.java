package cl.ubb.Pila;

import static org.junit.Assert.*;

import org.junit.Test;

public class PilaTest {
	
    @Test
    public void NuevaPilaVacia() { //Nueva Stack es vac�a
    	Pila stack = new Pila();
    	assertEquals(0,stack.getTamanio());	
    }
    
    @Test
    public void IngresaUno_PilaNoVacia() { //Al agregar el n�mero 1, stack no est� vac�a.
    	Pila stack = new Pila();
        stack.push(1);
        assertEquals(false,stack.esVacia());	
    }
    
    @Test
    public void IngresaUnoYDos_PilaNoVacia() { //Al agregar n�meros 1 y 2, stack no est� vac�a.
    	Pila stack = new Pila();
        stack.push(1);
        stack.push(2);
        assertEquals(false,stack.esVacia());	
    }
    
    @Test
    public void IngresaUnoYDos_TamanoDos() { //Al agregar n�meros 1 y 2, el tama�o de stack es 2.
    	Pila stack = new Pila();
        stack.push(1);
        stack.push(2);
        assertEquals(2,stack.getTamanio());	
    }
    
    @Test
    public void IngresaUno_HacePop_DevuelveUno() { //Al agregar n�mero 1 y hacer pop, stack devuelve el n�mero 1.
    	Pila stack = new Pila();
        stack.push(1);
        assertEquals(1,stack.pop());	
    }
    
    @Test
    public void IngresaUnoYDos_HacePop_DevuelveDos() { //Al agregar n�meros 1 y 2 hacer pop,  stack devuelve el n�mero 2.
    	Pila stack = new Pila();
        stack.push(1);
        stack.push(2);
        Pila aux = new Pila();
        aux.push(stack.pop());
        assertEquals(2,aux.inicio.getValor());	
    }
    
    @Test
    public void IngresaTresYCuatro_HacePopPop_DevuelveCuatro() { //Al agregar n�meros 3 y 4 hacer pop dos veces, stack devuelve 4 y 3.
    	Pila stack = new Pila();
        stack.push(3);
        stack.push(4);
        Pila aux = new Pila();
        aux.push(stack.pop());
        stack.pop();
        assertEquals(4,aux.inicio.getValor());	
    }
    
    @Test
    public void IngresaUno_HacePop_DevuelveTop() { //Al agregar n�meros 3 y 4 hacer pop dos veces, stack devuelve 4 y 3.
    	Pila stack = new Pila();
        stack.push(1);
        Pila aux = new Pila();
        aux.push(stack.pop());
        assertEquals(1,aux.top());	
    }
    
    @Test
    public void IngresaUnoYDos_HaceTop_DevuelveDos() { //Al agregar n�meros 3 y 4 hacer pop dos veces, stack devuelve 4 y 3.
    	Pila stack = new Pila();
        stack.push(1);
        stack.push(2);
        Pila aux = new Pila();
        aux.push(stack.top());
        assertEquals(2,aux.top());	
    }
    
    
}